<?php

//require_once("../app/core/Controller.php");

class Home extends Controller {
    public function index() {
        $data['title'] = 'home';
        $data['nama'] = $this->model('HomeModel')->getUser();
        $data['users'] = $this->model('UserModel')->getAllUser();

        $this->view('template/header', $data);
        $this->view("home/index", $data);
        $this->view('template/footer', $data);

    }
    public function about($company='smkn1'){
        $data['title']= 'about';
        $data['company']= $company;
        $this->view('template/header', $data);
        $this->view("home/index", $data);
        $this->view('template/footer', $data);
    }
    public function register()
    {
        $this->view('register/signup'); 
    }

    public function login()
    {
        $this->view('login/login');
    }
    public function store(){
        $data = [
            'username' => $_POST['username'],
            'email' => $_POST['email'],
            'first_name' => $_POST['firstname'],
            'last_name' => $_POST['lastname'],
            'password' => password_hash($_POST['password'], PASSWORD_BCRYPT),
        ];



        if ($this->model('UserModel')->register($data)){
            redirect('home/login');
        }

        back();
        
    }
    public function auth (){
        $data = [
            'username' => $_POST['username'],
            'password'=> $_POST['password'],
        ];

        $user = $this->model('UserModel')->getUserByUsername($data['username']);

        if ($user && password_verify($data['password'], $user['password'])){
            redirect('home/index');
        }
        back();
    }
}
