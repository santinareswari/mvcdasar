
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Signup</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sign-in/">
    <!-- Bootstrap core CSS -->
    <!-- <link href="http://localhost/mvcdsar/public/assets/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/5.0.0/mdb.min.css" rel="stylesheet"/>

    <link href="<?= BASE_URL .'css/bootstrap.min.css'?>" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    <!-- Custom styles for this template -->

    <link href="<?= BASE_URL .'css/signup.css'?>" rel="stylesheet">
</head>
<body class="text-center">

<img class="mb-4" src="https://seeklogo.com/images/B/bootstrap-5-logo-85A1F11F4F-seeklogo.com.png" alt="" width="72" height="57">

    <!-- Pills navs -->
<ul class="nav nav-pills mb-3" id="user" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="user-tab-1" data-mdb-toggle="pill" href="#user-register" role="tab" aria-controls="user-register" aria-selected="true">Register</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="user-tab-2" data-mdb-toggle="pill" href="#user-login" role="tab" aria-controls="user-login" aria-selected="false">Login</a>
  </li>
</ul>

<div class="tab-content" id="user-content">
  <div class="tab-pane fade show active" id="user-register" role="tabpanel" aria-labelledby="user-tab-1">
  <main class="form-signin">
        <form action="<?= BASE_URL .'home/store' ?>" method="POST">

            <div class="form-floating">
                <input type="text" name="username" class="form-control" id="floatingInput" placeholder="name@example.com">
                <label for="floatingInput">Username</label>
            </div>
            <div class="form-floating">
                <input type="text" name="firstname" class="form-control" id="floatingInput" placeholder="name@example.com">
                <label for="floatingInput">First name</label>
            </div>
            <div class="form-floating">
                <input type="text" name="lastname"class="form-control" id="floatingInput" placeholder="name@example.com">
                <label for="floatingInput">Last name</label>
            </div>
            <div class="form-floating">
                <input type="email" name="email" class="form-control" id="floatingInput" placeholder="name@example.com">
                <label for="floatingInput">Email</label>
            </div>
            <div class="form-floating">
                <input type="password" name="password" class="form-control" id="floatingInput" placeholder="name@example.com">
                <label for="floatingInput">Password</label>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit" name="button">Sign up</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2017–2021</p>
        </form>
    </main>
  </div>
  <div class="tab-pane fade" id="user-login" role="tabpanel" aria-labelledby="user-tab-2">
  <form method="POST">

            <div class="form-floating">
                <input type="text" class="form-control" id="floatingInput" placeholder="name@example.com">
                <label for="floatingInput">Username</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control" id="floatingInput" placeholder="name@example.com">
                <label for="floatingInput">Password</label>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit" name="button">Log in</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2017–2021</p>
        </form>
  </div>
</div>

<!-- MDB -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/5.0.0/mdb.min.js"></script>
</body>
</html>