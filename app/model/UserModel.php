<?php
class UserModel {
    private $table = 'users';
    private $db;

    private $name = 'Santi';
    public function getUser() {
        return $this->name;
    }

    public function __construct(){
        $this->db = new Database;
    }

    public function getAllUser() {
        $this->db->query("SELECT * FROM ($this->table)");
        return $this->db->resultAll();
    }

    public function getUserById($id) {
        $this->db->query("SELECT * FROM ($this->table) WHERE id = $id");
        $this->db->bind('id', '$id');
        return $this->db->resultSingle();
    
    }
    public function getUserByUsername($username) {
        $this->db->query("SELECT * FROM ($this->table) WHERE username = :username");
        $this->db->bind(':username', $username);
        return $this->db->resultSingle();
    
    }

     public function register($data) {
        //  $stmt = $this->db->query("SELECT * FROM ($this->table) WHERE email=: or username=:username");

        //  $this->db->bind('username', $data['username']);
        //  $this->db->bind('email', $data['email']);
        //  $row = $this->db->resultSingle();

         $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

         $this->db->query("INSERT INTO {$this->table} (username, first_name, last_name, email, password ) VALUES (:username, :first_name, :last_name, :email, :password)");
         
         $this->db->bind(":username", $data['username']);
         $this->db->bind(":first_name", $data['first_name']);
         $this->db->bind(":last_name", $data['last_name']);
         $this->db->bind(":email", $data['email']);
         $this->db->bind(":password", $data['password']);

         $this->db->execute();

         return $this->db->rowCount();

         
     }



     }



?>